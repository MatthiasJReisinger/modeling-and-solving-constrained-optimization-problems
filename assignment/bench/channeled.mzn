%------------------------------------------------------------------------------%
% clrp_nodeview_non_overlapping.mzn
%
% This file contains a MiniZinc model for the "Capacitated Location-Routing
% Problem". 
%------------------------------------------------------------------------------%

include "alldifferent_except_0.mzn";
include "at_least.mzn";
include "nvalue.mzn";
include "lex_less.mzn";
include "globals.mzn";

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The Model Parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

int: ncustomers;
int: ndepots;
int: vehicle_capacity;
array [1..ndepots] of int: capacities;
array [1..ncustomers] of int: demands;
array [1..ndepots] of int: opening_cost;
int: route_cost;
int: nlocations = ncustomers + ndepots;
array [1..nlocations,1..nlocations] of int: distances;

int: nvehicles;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The Core of the Model
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% The length of the tour of each vehicle.
array [1..nvehicles] of var 0..ncustomers: RouteLength;
% Holds for each vehicle the travelled route.
array [1..nvehicles,1..ncustomers] of var 0..ncustomers: Routes;
% Helper variable for convenience.
array [1..nvehicles] of var bool: VehicleActive;
% Denotes for each vehicle the depot at which its route is started.
array [1..nvehicles] of var 0..ndepots: StartDepot;
% Denotes for each vehicle the total number of goods it distributes on its route.
array [1..nvehicles] of var int: TransportedGoods;

% A vehicle is active iff it is assigned a route.
constraint
    forall (v in 1..nvehicles) (VehicleActive[v] <-> RouteLength[v] > 0);

% A vehicle is active iff it transports goods.
constraint
    forall (v in 1..nvehicles) (VehicleActive[v] <-> TransportedGoods[v] > 0);

% Make sure the elements that go beyond a vehicle's tour are set to 0.
constraint
    forall (v in 1..nvehicles,i in 1..ncustomers)
        (Routes[v,i] > 0 <-> i <= RouteLength[v]);

% Makes sure that each customer is visited.
% NOTE: The formal model in the report makes uses of the `atleast_nvalue`
%       to formulate this. However, MiniZinc does not support this constraint,
%       therefore we have to express this behaviour via `nvalue (...) >= ...`.
constraint
    nvalue ([Routes[v,i] | v in 1..nvehicles, i in 1..ncustomers]) >= ncustomers;

% Makes sure that each customer is visited at most once.
constraint
    alldifferent_except_0 ([Routes[v,i] | v in 1..nvehicles, i in 1..ncustomers]);

% Only active vehicles are assigned a depot.
constraint
    forall (v in 1..nvehicles) (StartDepot[v] > 0 <-> VehicleActive[v]);

% Convenience constraint
% TODO maybe the range of TransportedGoods can be set to >= 0 when declared
constraint
    forall (v in 1..nvehicles) (TransportedGoods[v] >= 0);

% The number of goods transported by a vehicle is equal to the demands of the
% clients visited on its route.
constraint
    forall (v in 1..nvehicles where VehicleActive[v])
        (TransportedGoods[v]
            == sum (i in 1..ncustomers where i <= RouteLength[v])
                (demands[Routes[v,i]]));

% Respect the vehicle capacities
constraint
    forall (v in 1..nvehicles) (TransportedGoods[v] <= vehicle_capacity);

% Respect the depot capacities.
constraint
    forall (d in 1..ndepots)
        (sum (v in 1..nvehicles where StartDepot[v] == d)
            (TransportedGoods[v]) <= capacities[d]);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Symmetry Breaking
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Symmetry breaking: Assures that reversed routes are not reconsidered.
constraint
    forall (v in 1..nvehicles where VehicleActive[v])
        (Routes[v,1] <= Routes[v,RouteLength[v]]);

% Symmetry breaking
constraint
    forall (v1 in 1..nvehicles,v2 in 1..nvehicles where VehicleActive[v1] /\ VehicleActive[v2])
        (v1 < v2 <-> Routes[v1,1] < Routes[v2,1]);

% Symmetry breaking
constraint
    forall (v1 in 1..nvehicles,v2 in 1..nvehicles)
        (v1 < v2 /\ VehicleActive[v2] -> VehicleActive[v1]);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Redundant Constraints
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Redundant constraint.
constraint
    sum ([TransportedGoods[v] | v in 1..nvehicles])
        == sum ([demands[c] | c in 1..ncustomers]);

% Redundant constraint.
constraint
    sum ([TransportedGoods[v] | v in 1..nvehicles])
        <= sum ([capacities[d] | d in 1..ndepots]);

% Redundant constraint.
constraint
    sum ([RouteLength[v] | v in 1..nvehicles]) == ncustomers;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Defining the Objective Function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Helper variable for convenience.
array [1..ndepots] of var bool: DepotOpen;

% A depot is open iff at least one vehicle starts its route there.
constraint
    forall (d in 1..ndepots)
        (DepotOpen[d] == exists (v in 1..nvehicles) (StartDepot[v] == d));

% Represents the overall opening costs of all opened facilities.
var int: TotalOpeningCosts;

constraint
    TotalOpeningCosts == sum (d in 1..ndepots where DepotOpen[d]) (opening_cost[d]);

% Represents the overall costs of using the vehicles.
var int: TotalVehicleUsageCosts;

constraint
    TotalVehicleUsageCosts == sum (v in 1..nvehicles where VehicleActive[v])
        (route_cost);

% Holds for each vehicle the cost of the travelled route
array [1..nvehicles] of var int: RouteCosts;

constraint
    forall (v in 1..nvehicles where not VehicleActive[v]) (RouteCosts[v] == 0);

constraint
    forall (v in 1..nvehicles where VehicleActive[v])
    (
        let {var 1..nlocations: StartDepotIndex = ncustomers + StartDepot[v]}
        in RouteCosts[v] == distances[StartDepotIndex,Routes[v,1]]
            + distances[Routes[v,RouteLength[v]],StartDepotIndex]
            + sum (i in 2..ncustomers where i <= RouteLength[v])
                (distances[Routes[v,i-1],Routes[v,i]])
    );

% Represents the overall costs for all routes.
var int: TotalRouteCosts;

constraint
    TotalRouteCosts == sum (v in 1..nvehicles) (RouteCosts[v]);

% Used to define the objective function to be minimized.
var int: TotalCosts;

% Defines the objective function.
constraint
    TotalCosts == TotalOpeningCosts + TotalVehicleUsageCosts + TotalRouteCosts;

%solve minimize TotalCosts;
solve satisfy;

