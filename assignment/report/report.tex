\documentclass[11pt]{article}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[usenames,dvipsnames]{color}
\usepackage{lscape}
\usepackage{amsmath,relsize}
\usepackage{amssymb}
\usepackage{amstext}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{thmtools}
\usepackage{stmaryrd}
\usepackage{bussproofs}
\usepackage{stackengine}
\usepackage{listings}
\usepackage{enumitem}
\usepackage{color}
\usepackage[svgnames]{xcolor}
\usepackage{pgfplots}
\usepackage{tikz}
\usetikzlibrary{calc,trees,positioning,arrows,chains,shapes.geometric,%
    decorations.pathreplacing,decorations.pathmorphing,shapes,%
    matrix,shapes.symbols,positioning,fit,backgrounds}
\usepackage{caption}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage[top=1.5in, bottom=1.5in, left=1.3in, right=1.3in]{geometry}
\usepackage{booktabs}

% Define custom colors
\definecolor{D4Red}{named}{Red}
\definecolor{D4LightRed}{named}{Salmon}
\definecolor{D4Green}{named}{Green}
\definecolor{D4LightGreen}{named}{LightGreen}
\definecolor{D4Blue}{named}{RoyalBlue}
\definecolor{D4LightBlue}{named}{LightBlue}

\lstset{
  mathescape=true,
  backgroundcolor=\color{white},
  basicstyle=\small\ttfamily,
  columns=flexible,
  breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
  breaklines=true,                 % sets automatic line breaking
  captionpos=b,                    % sets the caption-position to bottom
  commentstyle=\color{mygreen},    % comment style
  escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
  keepspaces=true,
  keywordstyle=\color{Black},       % keyword style
  language=Java,                 % the language of the code
  numbers=left,
  numbersep=5pt,                   % how far the line-numbers are from the code
  numberstyle=\tiny\color{mygray}, % the style that is used for the line-numbers
  stringstyle=\color{mymauve},     % string literal style
  tabsize=4,                       % sets default tabsize to 2 spaces
}

\newenvironment{exercise}{\itshape}{\vspace{0.3cm}}
\newcommand{\mlstinline}[1]{\text{\lstinline!#1!}}

\allowdisplaybreaks[1]
\setlist[enumerate,1]{leftmargin=2cm}

%% caption style for figures
%\captionsetup[figure]{
%singlelinecheck=true,
%margin=0pt,
%format=plain,
%labelfont={bf,normalsize},
%%font={small},
%labelsep=colon,
%justification=justified,
%minmargin=0.1\textwidth
%}

% Bar chart dimensions
\newcommand{\barwidth}{6pt}
\newcommand{\barpadding}{2pt}
\newcommand{\chartheight}{7cm}
\newcommand{\enlargexlimits}{0.06}

\makeatletter
\makeatother

\bibliographystyle{acm}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% BODY
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\begin{center}
\textbf{\LARGE{Assignment Report}}\\
\textbf{\large{Modeling \& Solving Constrained Optimization Problems}}\\
\end{center}

\begin{center}
Matthias Reisinger, 1025631
\end{center}

\vspace{8pt}


\section{Introduction}

In the context of this work the \emph{capacitated location routing} had to be
modeled based on constraint programming. In a first step we introduced two
different formal models. Later on these models were implemented via MiniZinc
and evaluated via the supplied benchmarks.

The rest of this work is organized as follows. The capacitated location routing
problem is introduced in section \ref{sec:problem-spec}. In section
\ref{sec:edgemodel} we introduce a first formal model based on constraint
programming. Section \ref{sec:nodemodel} presents an alternative model which has
been refined and optimized via symmetry breaking and by adding redundant
constraints as described in sections \ref{sec:symmetries} and
\ref{sec:redundancies}. Section \ref{sec:channelling} describes how the presented
models can be combined via channelling. Finally, in sections \ref{sec:implementation}
and \ref{sec:evaluation} we give an overview of the implementation and
present the results of an experimental evaluation.

\section{Problem Statement}
\label{sec:problem-spec}

We define the capacitated location routing problem based on the notions in the
assignment specification and the works of Prins et al.\ \cite{Prins:2006} and
Harks et al.\ \cite{Harks:2013}.
Given is an undirected graph $G = (L,A)$ with nodes $L = C \cup F$ and arcs $A$.
The nodes represent locations, where $C$ denotes a set of customers and $F$ a
set of depots or facilities. In the context of this assignment all nodes are
connected, i.e. $(l_1,l_2) \in A$ for all $l_1, l_2 \in L$. The euclidean
distance between two nodes $l_1, l_2 \in L$ is given by $e_{l_1 l_2}$. For each
client $c \in C$ the demand $d_c$ is known. The opening costs for each facility
$f \in F$ is given by $O_f$. Furthermore, there is a set of vehicles $V$. For
each vehicle there is a fixed usage cost $U$. Finally, the number of goods held
by a facility $f \in F$ is restricted by its capacity $w_f$. The capacity of
vehicles is uniformly $Q$.

Informally, the aim is to find a set of routes such that the demand of each
client is met, minimizing the total cost for routes and opening depots.
Moreover, we require that routes must no overlap each other which implies
that each customer is part of exactly one route. Each route is assigned to
exactly one vehicle and forms a cycle that starts and ends at the facility
whose goods are used to serve the customers that are part of the route.
Furthermore, the routes have to respect the capacities of vehicles and depots.

\section{A First Model}
\label{sec:edgemodel}

Let the boolean variable $x_{v l_1 l_2}$ denote whether there is a connection
from location $l_1 \in L$ to $l_2 \in L$ on the route that is assigned to
vehicle $v \in V$. The constraints of capacitated location routing can then by formalized as follows.

\begin{align}
    &\mathit{exactly}(1, \lbrace x_{v l c} \mid v \in V, l \in L \rbrace, \mathit{true}) & & \forall c \in C \label{eq:edgemodel_demands_met}\\
    &\exists l' \in L : x_{v l l'} \iff \exists l' \in L : x_{v l' l} & & \forall v \in V, \forall l \in L \label{eq:edgemodel_continuity}\\
    &\mathit{exactly}(1, \lbrace x_{v l f} \mid l \in L, f \in F \rbrace, \mathit{true}) & & \forall v \in V \label{eq:edgemodel_start_depot}\\
    &p_v = \sum_{c \in \lbrace c' \in C \mid \exists l \in L : x_{v l c'} \rbrace } d_c & & \forall v \in V \label{eq:edgemodel_total_delivery}\\
    &p_v \leq Q & & \forall v \in V\label{eq:edgemodel_vehicle_capacity}\\
    &\sum_{v \in \lbrace v' \in V \mid \exists c \in C : x_{v' f c}\rbrace} p_v \leq w_f & & \forall f \in F \label{eq:edgemodel_depot_capacity}\\
    &\exists s \in S, \exists l \in L \setminus S : x_{v s l} & & \forall v \in V, \forall \emptyset \subset S \subset L \label{eq:edgemodel_subtour}
\end{align}

% TODO cycles with depots

Equation \ref{eq:edgemodel_demands_met} ensures that each customer is visited
exactly once. Equation \ref{eq:edgemodel_continuity} asserts that every vehicle
that visits a customer also has to leave it again, which assures the continuity
of the routes. A vehicle visits only one facility, namely the facility where it
starts its route, which is expressed in constraint
\ref{eq:edgemodel_start_depot}. In equations \ref{eq:edgemodel_total_delivery}
and \ref{eq:edgemodel_vehicle_capacity} we ensure that each vehicle serves the
demands of all the customers on its route without violating the vehicle's
capacity. Equation \ref{eq:edgemodel_depot_capacity} assures that the capacities
of the depots are not exceeded. The constraint in equation
\ref{eq:edgemodel_subtour} embodies subtour elimination.

The practicability of the last constraint is the main flaw of this model. It
considers all the subsets of the set of locations $L$ which are
exponentially many. We therefore tried to reformulate the problem which led to
the model definition in the next section.

\section{Changing the Perspective}
\label{sec:nodemodel}

To represent a vehicle's route, let $r_{v i} \in C \cup \lbrace \varepsilon
\rbrace$ denote the customer that vehicle $v \in V$ visits at the $i$-th stop on
its route, where $1 \leq i \leq \left\vert C \right\vert$ and $\varepsilon$
denotes an undefined element. Let $\mathit{n}_v$ denote the route length of
vehicle $v \in V$. Intuitively, $\lbrace r_{v i} \mid 1 \leq i \leq n_v \rbrace$
represents the route that is assigned to vehicle $v \in V$. For all $i > n_v$, the
according $r_{v i}$ will be undefined, denoted by $\varepsilon$. For a vehicle
$v \in V$, $s_v \in F \cup \lbrace \varepsilon \rbrace$ will denote the facility
where $v$'s tour starts. Furthermore, we will use the boolean variable $y_f$ to
denote whether a facility $f \in F$ has to be opened. $a_v$ denotes whether
vehicle $v \in V$ is active.

\begin{align}
    & r_{v i} \neq \varepsilon \iff i \leq n_v & & \forall v \in V, \forall 1 \leq i \leq \left\vert C \right\vert\\
    & \mathit{atleast\_nvalue}(\left\vert C \right\vert, \lbrace r_{v i} \mid v \in V, 1 \leq i \leq \left\vert C \right\vert \rbrace)\\
    & \mathit{alldifferent}(\lbrace r_{v i} \mid v \in V, 1 \leq i \leq \left\vert C \right\vert, r_{v i} \neq \varepsilon \rbrace)\\
    & a_v \iff n_v > 0 & & \forall v \in V\\
    & a_v \iff p_v > 0 & & \forall v \in V\\
    & s_v \neq \varepsilon \iff a_v & & \forall v \in V\\
    & p_v = \sum_{i = 1}^{n_v} d_{r_{v i}} & & \forall v \in V \label{eq:nodemodel_total_delivery}\\
    & p_v \leq Q & & \forall v \in V\label{eq:nodemodel_vehicle_capacity}\\
    & \sum_{v \in \lbrace v' \in V \mid s_{v'} = f \rbrace} p_v \leq w_f & & \forall f \in F \label{eq:nodemodel_depot_capacity}\\
    & y_f \iff \exists v \in V : s_v = f & & \forall f \in F\\
\end{align}

The objective function can then be formulated as stated in equation \ref{eq:nodemodel_objective}. The first term represents the total opening costs for the used facilities and the second term denotes the costs per route or vehicle respectively (in particular, $e_{s_v r_{v 1}}$ denotes the euclidean cost of the edge between the start depot $s_v$ of vehicle $v \in V$ and the first customer $r_{v 1}$ visited by $v$).

\begin{align}
    & \sum_{f \in \lbrace f' \in F \mid y_{f'} \rbrace} O_f
        + \sum_{v \in \lbrace v' \in V \mid a_v \rbrace}
            (U + e_{s_v r_{v 1}} + e_{r_{v n_v} s_v}
            + \sum_{i = 1}^{n_v - 1} e_{r_{v i} r_{v (i+1)}})
            \label{eq:nodemodel_objective}
\end{align}


\section{Adding Redundancies}
\label{sec:redundancies}

To support the process of constant propagation, a number of redundant
constraints were added to the model which can help to prune the search space:

\begin{align}
    &\sum_{v \in V} p_v = \sum_{c \in C} d_c \label{eq:redundant_demands}\\
    &\sum_{v \in V} p_v \leq \sum_{f \in F} w_f \label{eq:redundant_facility_capacities}\\
    &\sum_{v \in V} n_v = \left\vert C \right\vert \label{eq:redundant_route_length}
\end{align}

Constraint \ref{eq:redundant_demands} denotes that the amount of goods
transported by all vehicles has to be equal to the demands of all customers. In
constraint \ref{eq:redundant_facility_capacities} we express that the total
amount of transported goods must not exceed the total capacity of all
facilities. Constraint \ref{eq:redundant_route_length} states that the total
number of customers has to be equal to the number of visited customers.

% TODO TransportedGoods[v] >= 0 for all v


\section{Breaking Symmetries}
\label{sec:symmetries}

The constraints defined so far allow various symmetrical
solutions that are equal with respect to the problem definition. In particular
it does not matter \ldots

\begin{itemize}
    \item \ldots in which direction a route is considered and
    \item \ldots which route is executed by which vehicle
\end{itemize}

Therefore the following constraints were introduced which assume total
orders $\prec_C$ and $\prec_V$ on customers $C$ and vehicles $V$:

\begin{align}
    &r_{v 1} \prec_C r_{v n_v} & & \forall v \in V \label{eq:symm_reverse}\\
    &v_1 \prec_V v_2 \wedge a_{v_1} \wedge a_{v_2} \implies r_{v_1 1} \prec_V r_{v_2 1} & & \forall v_1,v_2 \in V \label{eq:symm_vehicle1}\\
    &v_1 \prec_V v_2 \wedge a_{v_2} \implies a_{v_1} & & \forall v_1,v_2 \in V \label{eq:symm_vehicle2}
\end{align}

Constraint \ref{eq:symm_reverse} prevents that routes are reconsidered in reversed direction. Constraints \ref{eq:symm_vehicle1} and \ref{eq:symm_vehicle2} remove solutions where the same routes are assigned to different vehicles.


\section{Channelling}
\label{sec:channelling}

The defined models can be combined via the following channelling constraints:

\begin{align}
    & x_{v c_1 c_2} \iff \exists 1 \leq i \leq n_v - 1 : r_{v i} = c_1 \wedge r_{v (i+1)} = c_2 & & \forall v \in V, \forall c_1,c_2 \in C\\
    & x_{v f c} \iff s_v = f \wedge r_{v 1} = c & & \forall v \in V, \forall c \in C, \forall f \in F\\
    & x_{v c f} \iff s_v = f \wedge r_{v n_v} = c & & \forall v \in V, \forall c \in C, \forall f \in F
\end{align}


\section{Implementation}
\label{sec:implementation}

The presented models and the proposed refinements were implemented based on
MiniZinc. The models are bundled into two source files:

\begin{itemize}
    \item \lstinline!clrp.mzn! contains the model from section
        \ref{sec:nodemodel} with the refinements from sections
        \ref{sec:redundancies} and \ref{sec:symmetries}.
    \item \lstinline!clrp_channelling.mzn! additionally contains
        the model presented in \ref{sec:edgemodel} and connects
        the models via the channelling constraints from section
        \ref{sec:channelling}.
\end{itemize}

\section{Evaluation}
\label{sec:evaluation}

The system used for performing the tests comprised a 3.4 GHz
\emph{Intel\textsuperscript{\textregistered}~Core\textsuperscript{\texttrademark}~i7}
quad-core processor and 16 GB RAM, running an Ubuntu 14.04.1 LTS platform in 64 bit mode.
The benchmarks were run via MiniZinc's command line interface, using its \verb!mzn-gecode!
command. The command line option \verb!-s! was used to obtain statistical output.
Furthermore, the number of vehicles was fixed to 11 by passing the command line option
\verb!-D nvehicles=11!. Since also the minimization process did not finish within
an appropriate amount of time for any of the benchmarks, we evaluated our models
based on the search for satisfying solutions (\lstinline!solve satisfy!).
In the following we will only show the results for those benchmarks, for which we could
obtain results within a appropriate amount of time. We measured the number of nodes
that were explored until a satisfying solution was found, as well as the according
run-times and solve-times.

In the following, \emph{base + redundancies} will denote the model that was implemented
based on the constraints in sections \ref{sec:nodemodel} and \ref{sec:redundancies}, and
to denote the symmetry breaking refinements from section \ref{sec:symmetries} we will use
the notions \emph{symmetries1}, \emph{symmetries2} and \emph{symmetries3}. However, we
were not able to obtain results with the channelled models.

\begin{figure}[h]
    \centering
    \begin{tikzpicture}
        \begin{axis}[
            width=\textwidth,
            height=\chartheight,
            ymin=0,
            ymax=100000,
            ybar=2pt,
            ymode=log,
            bar width=8pt,
            enlarge x limits=\enlargexlimits,
            ymajorgrids=true,
            symbolic x coords={coord20-5-1,coord20-5-1b,coord20-5-2,coord20-5-2b,coord50-5-1,coordGaspelle},
            tick style={semithick,color=black},
            xtick={coord20-5-1,coord20-5-1b,coord20-5-2,coord20-5-2b,coord50-5-1,coordGaspelle},
            scaled y ticks=false,
            tick label style={font=\small},
            x tick label style={rotate=45,font=\footnotesize},
            major x tick style=transparent,
            legend cell align=left,
            legend pos=north east,
            legend style={font=\scriptsize},
        ]

            \addplot[style={D4Blue,fill=D4Blue,mark=none}]
            coordinates {(coord20-5-1,11568) (coord20-5-1b,242) (coord20-5-2,276) (coord20-5-2b,48074) (coord50-5-1,0) (coordGaspelle,0)};

            \addplot[style={D4Red,fill=D4Red,mark=none}]
            coordinates {(coord20-5-1,14010) (coord20-5-1b,395) (coord20-5-2,132) (coord20-5-2b,8471) (coord50-5-1,0) (coordGaspelle,0)};

            \addplot[style={Purple,fill=Purple,mark=none}]
            coordinates {(coord20-5-1,432) (coord20-5-1b,445) (coord20-5-2,158) (coord20-5-2b,18136) (coord50-5-1,518) (coordGaspelle,347)};

            \addplot[style={D4Green,fill=D4Green,mark=none}]
            coordinates {(coord20-5-1,434) (coord20-5-1b,513) (coord20-5-2,158) (coord20-5-2b,9310) (coord50-5-1,518) (coordGaspelle,537)};

            \legend{base + redundancies, symmetries1, symmetries2, symmetries3}
        \end{axis}
    \end{tikzpicture}
    \caption{Number of nodes as listed in table \ref{tab:nodes}.}
\end{figure}

\begin{figure}[h]
    \centering
    \begin{tikzpicture}
        \begin{axis}[
            width=\textwidth,
            height=\chartheight,
            ymin=0,
            ymax=100000,
            ybar=2pt,
            ymode=log,
            bar width=8pt,
            enlarge x limits=\enlargexlimits,
            ymajorgrids=true,
            symbolic x coords={coord20-5-1,coord20-5-1b,coord20-5-2,coord20-5-2b,coord50-5-1,coordGaspelle},
            tick style={semithick,color=black},
            xtick={coord20-5-1,coord20-5-1b,coord20-5-2,coord20-5-2b,coord50-5-1,coordGaspelle},
            scaled y ticks=false,
            tick label style={font=\small},
            x tick label style={rotate=45,font=\footnotesize},
            major x tick style=transparent,
            legend cell align=left,
            legend pos=north east,
            legend style={font=\scriptsize},
        ]

            \addplot[style={D4Blue,fill=D4Blue,mark=none}]
            coordinates {(coord20-5-1,3253) (coord20-5-1b,748) (coord20-5-2,771) (coord20-5-2b,14953) (coord50-5-1,0) (coordGaspelle,0)};

            \addplot[style={D4Red,fill=D4Red,mark=none}]
            coordinates {(coord20-5-1,2378) (coord20-5-1b,695) (coord20-5-2,662) (coord20-5-2b,2987) (coord50-5-1,0) (coordGaspelle,0)};

            \addplot[style={Purple,fill=Purple,mark=none}]
            coordinates {(coord20-5-1,743) (coord20-5-1b,723) (coord20-5-2,662) (coord20-5-2b,5216) (coord50-5-1,6752) (coordGaspelle,6554)};

            \addplot[style={D4Green,fill=D4Green,mark=none}]
            coordinates {(coord20-5-1,725) (coord20-5-1b,716) (coord20-5-2,658) (coord20-5-2b,2994) (coord50-5-1,6554) (coordGaspelle,799)};

            \legend{base + redundancies, symmetries1, symmetries2, symmetries3}
        \end{axis}
    \end{tikzpicture}
    \caption{Run-times in milliseconds as listed in table \ref{tab:run-time}.}
\end{figure}

\begin{figure}[h]
    \centering
    \begin{tikzpicture}
        \begin{axis}[
            width=\textwidth,
            height=\chartheight,
            ymin=0,
            ymax=100000,
            ybar=2pt,
            ymode=log,
            bar width=8pt,
            enlarge x limits=\enlargexlimits,
            ymajorgrids=true,
            symbolic x coords={coord20-5-1,coord20-5-1b,coord20-5-2,coord20-5-2b,coord50-5-1,coordGaspelle},
            tick style={semithick,color=black},
            xtick=data,
            xtick={coord20-5-1,coord20-5-1b,coord20-5-2,coord20-5-2b,coord50-5-1,coordGaspelle},
            scaled y ticks=false,
            tick label style={font=\small},
            x tick label style={rotate=45,font=\footnotesize},
            major x tick style=transparent,
            legend cell align=left,
            legend pos=north east,
            legend style={font=\scriptsize},
        ]

            \addplot[style={D4Blue,fill=D4Blue,mark=none}]
            coordinates {(coord20-5-1,2686) (coord20-5-1b,182) (coord20-5-2,179) (coord20-5-2b,13990) (coord50-5-1,0) (coordGaspelle,0)};

            \addplot[style={D4Red,fill=D4Red,mark=none}]
            coordinates {(coord20-5-1,1179) (coord20-5-1b,116) (coord20-5-2,79) (coord20-5-2b,2405) (coord50-5-1,0) (coordGaspelle,0)};

            \addplot[style={Purple,fill=Purple,mark=none}]
            coordinates {(coord20-5-1,145) (coord20-5-1b,118) (coord20-5-2,83) (coord20-5-2b,4616) (coord50-5-1,2928) (coordGaspelle,134)};

            \addplot[style={D4Green,fill=D4Green,mark=none}]
            coordinates {(coord20-5-1,144) (coord20-5-1b,127) (coord20-5-2,85) (coord20-5-2b,2396) (coord50-5-1,2966) (coordGaspelle,165)};

            \legend{base + redundancies, symmetries1, symmetries2, symmetries3}
        \end{axis}
    \end{tikzpicture}
    \caption{Solve-times in milliseconds as listed in table \ref{tab:solve-time}.}
\end{figure}

\begin{figure}[h]
    \centering
    \begin{tikzpicture}
        \begin{axis}[
            width=\textwidth,
            height=\chartheight,
            ymin=0,
            ybar=2pt,
            ymode=log,
            bar width=8pt,
            enlarge x limits=\enlargexlimits,
            ymajorgrids=true,
            symbolic x coords={coord20-5-1,coord20-5-1b,coord20-5-2,coord20-5-2b,coord50-5-1,coordGaspelle},
            tick style={semithick,color=black},
            xtick=data,
            xtick={coord20-5-1,coord20-5-1b,coord20-5-2,coord20-5-2b,coord50-5-1,coordGaspelle},
            scaled y ticks=false,
            tick label style={font=\small},
            x tick label style={rotate=45,font=\footnotesize},
            major x tick style=transparent,
            legend cell align=left,
            legend pos=north east,
            legend style={font=\scriptsize},
        ]

            \addplot[style={D4Blue,fill=D4Blue,mark=none}]
            coordinates {(coord20-5-1,1526037) (coord20-5-1b,173626) (coord20-5-2,188013) (coord20-5-2b,6528713) (coord50-5-1,0) (coordGaspelle,0)};

            \addplot[style={D4Red,fill=D4Red,mark=none}]
            coordinates {(coord20-5-1,1646252) (coord20-5-1b,205981) (coord20-5-2,181157) (coord20-5-2b,1312932) (coord50-5-1,0) (coordGaspelle,0)};

            \addplot[style={Purple,fill=Purple,mark=none}]
            coordinates {(coord20-5-1,235836) (coord20-5-1b,210470) (coord20-5-2,184456) (coord20-5-2b,2779569) (coord50-5-1,1684971) (coordGaspelle,1690374)};

            \addplot[style={D4Green,fill=D4Green,mark=none}]
            coordinates {(coord20-5-1,236344) (coord20-5-1b,218561) (coord20-5-2,184341) (coord20-5-2b,1512770) (coord50-5-1,1690374) (coordGaspelle,244440)};

            \legend{base + redundancies, symmetries1, symmetries2, symmetries3}
        \end{axis}
    \end{tikzpicture}
    \caption{Solve-times in milliseconds as listed in table \ref{tab:solve-time}.}
\end{figure}

\begin{table}[h]
    \centering
    \begin{tabular}{l r r r r}
        \toprule
        Benchmark & base + redundancies & symmetry1 & symmetry2 & symmetry3 \\
        \midrule
        coord20-5-1   & 11568 & 14010 &   432 &  434 \\
        coord20-5-1b  &   242 &   395 &   445 &  513 \\
        coord20-5-2   &   276 &   132 &   158 &  158 \\
        coord20-5-2b  & 48074 &  8471 & 18136 & 9310 \\
        coord50-5-1   &   --- &   --- &   518 &  518 \\
        coordGaspelle &   --- &   --- &   347 &  537 \\
        \bottomrule
    \end{tabular}
    \caption{Number of nodes.}
    \label{tab:nodes}
\end{table}

\begin{table}[h]
    \centering
    \begin{tabular}{l r r r r}
        \toprule
        Benchmark & base + redundancies & symmetry1 & symmetry2 & symmetry3 \\
        \midrule
        coord20-5-1   &  3253 & 2378 &  743 &  725 \\
        coord20-5-1b  &   748 &  695 &  723 &  716 \\
        coord20-5-2   &   771 &  662 &  662 &  658 \\
        coord20-5-2b  & 14953 & 2987 & 5216 & 2994 \\
        coord50-5-1   &   --- &  --- & 6752 & 6554 \\
        coordGaspelle &   --- &  --- &  796 &  799 \\
        \bottomrule
    \end{tabular}
    \caption{Run-times in milliseconds.}
    \label{tab:run-time}
\end{table}

\begin{table}[h]
    \centering
    \begin{tabular}{l r r r r}
        \toprule
        Benchmark & base + redundancies & symmetry1 & symmetry2 & symmetry3 \\
        \midrule
        coord20-5-1   &  2686 & 1179 &  145 &  144 \\
        coord20-5-1b  &   182 &  116 &  118 &  127 \\
        coord20-5-2   &   179 &   79 &   83 &   85 \\
        coord20-5-2b  & 13990 & 2405 & 4616 & 2396 \\
        coord50-5-1   &   --- &  --- & 2928 & 2966 \\
        coordGaspelle &   --- &  --- &  134 &  165 \\
        \bottomrule
    \end{tabular}
    \caption{Solve-times in milliseconds.}
    \label{tab:solve-time}
\end{table}

\begin{table}[h]
    \centering
    \begin{tabular}{l r r r r}
        \toprule
        Benchmark & base + redundancies & symmetry1 & symmetry2 & symmetry3 \\
        \midrule
        coord20-5-1   & 1526037 & 1646252 &  235836 &  236344 \\
        coord20-5-1b  &  173626 &  205981 &  210470 &  218561 \\
        coord20-5-2   &  188013 &  181157 &  184456 &  184341 \\
        coord20-5-2b  & 6528713 & 1312932 & 2779569 & 1512770 \\
        coord50-5-1   &     --- &     --- & 1684971 & 1690374 \\
        coordGaspelle &     --- &     --- &  216412 &  244440 \\
        \bottomrule
    \end{tabular}
    \caption{Number of propagations.}
    \label{tab:propagations}
\end{table}


\clearpage
\bibliography{references}

\end{document}
