%------------------------------------------------------------------------------%
% wlp.mzn
%
% This file contains a MiniZinc model for the "Warehouse Location Problem". 
%------------------------------------------------------------------------------%

include "count_eq.mzn";

% Parameters to be passed to the model
int: Warehouses;
int: Stores;
array [1..Warehouses] of int: Capacity;
array [1..Warehouses] of int: FixedCost;
array [1..Stores] of int: Goods;
array [1..Stores,1..Warehouses] of int: SupplyCost;

% Contains for each pair (S,W) the quantitiy of goods moved from the warehouse
% W to the store S.
array [1..Stores,1..Warehouses] of var int: TransportedGoods;

% Indicates for each warehouse whether it is to be opened or not.
array [1..Warehouses] of var bool: WarehouseOpened;

% The number of transported goods has to be positive for each store.
constraint
    forall (s in 1..Stores,w in 1..Warehouses)
        (TransportedGoods[s,w] >= 0);

% Ensures that he total quantity of goods taken from a warehouse does not
% exceed its capacity.
constraint
    forall (w in 1..Warehouses)
        (sum (s in 1..Stores) (TransportedGoods[s,w]) <= Capacity[w]);

% Ensures that the total quantity of goods brought to a store is exactly
% equal to its request.
constraint
    forall (s in 1..Stores)
        (sum (w in 1..Warehouses) (TransportedGoods[s,w]) == Goods[s]);

% A warehouse is opened iff goods are taken from it.
constraint
    forall (w in 1..Warehouses)
        (
            let { var int: TakenGoods = sum (s in 1..Stores) (TransportedGoods[s,w]) }
            in WarehouseOpened[w] <-> TakenGoods > 0
        );

var int: OpeningCost;
var int: TransportCost;
% Used to define the objective function to be minimized.
var int: TotalCost;

constraint
    OpeningCost == sum (w in 1..Warehouses)
        (if WarehouseOpened[w] then FixedCost[w] else 0 endif);

constraint
    TransportCost == sum (s in 1..Stores,w in 1..Warehouses)
        (TransportedGoods[s,w] * SupplyCost[s,w]);

constraint
    TotalCost == OpeningCost + TransportCost;

% Each store is supplied by extactly one warehouse
constraint
    forall (s in 1..Stores)
        (count_eq(row(TransportedGoods, s), 0, Warehouses - 1));

solve satisfy;
